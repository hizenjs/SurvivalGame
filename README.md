# Game Design Document



![G4qtacX.jpg](C:\Users\Hizen\Desktop\G4qtacX.jpg)



 **Name of the game:** deded

**Stackholders:**

* Developer:

* 3D Artist:

* Level Designer:

* Design:

* QA:



## Game Overview

##### Game Concept:
A Horror-Survival game inside a city with mist.

##### Gender:
Survival Coop

##### Target Audiance:
Pc Players

##### Game Flow Summary:
Third Person view
Player use Keyboard and Mouse input to move.
Player can use Mouse inside HUD

##### Look and Feel:
Anxious, Distressing, Agonizing

Stylized



## Gameplay and Mechanics:

##### Game Progression:
continuous

##### Mission / Challenge Structure:
Survive the longest time possible

##### Puzzle Structure:
none

##### Objectives:
Survive the longest time possible

##### Play Flow:
The players start on the top of a building;
he should exploring the street of the city to fine materials and ressources in order to survive.
Can survive with an other pleyer to have better chance to survive

The player has food and dring management

The player has mental health management

##### Mechanics:
* The player need to go inside the mist to collect ressources
* Collected Ressources respawn in the world after 3 Hours
* The mental health of the player is affected when players is inside mist.
* Player can be attacked when is inside dark areas.
* With ressources, the player can build on the roof top of building and explore any other ones at proximity.
* Ennemies has affected by light with a decreased speed
* The player can craft makeshift weapons
* The player can find a dog who can be adopted to help the player

##### Physics:
Same as IRL

##### Objects:
The player can pick an item on pressing interraction input or by accessing inventory vicinity section.
For all items inside inventory, the player can use it by pressing the use button or drop/Split it by pressing the drop button.

##### Actions:
* The player can communicate in chat by pressing the chat button or vocally by pressing Mic Button
* The player can interract with his own inventory
* The player can interract with interractible object like Crafting table, Door, or lamp...
* ...


##### Combat:
* Ennemies attack the player when is inside the mist or at night
* Creatures attack with its limb.
* Creatures has distance attack.
* The player can attack creatures with crafted weapons, and traps.

##### Economy:
None

##### Screen Flow:
Looks like Fortnite and Sea of Thieves :)

##### Game Option:
* Keyboard Mapping
* Graphicals options
* Language
* Visual Settings
* Network Settings
* Audio Settings

##### Replaying and Saving:
* Each players has his inventory saved on the cloud
* The level is saved on each player has host.
* Parameters has saved on player computer.

##### Cheats and Easter Eggs:
Not Yet

## Story, Setting and Character:

##### Story and Narrative:

##### Game World:

##### General look and feel of world:

##### Areas:
* Roof top of buildings
* Underground
* Stores

##### Characters (Aspect):
possible class:
* Medics
* Engineer
* Survivalist
* Fighter


## Levels:

##### Levels:
City based on Queens/Boston

##### Training Level:
None


## Interface:

##### Visual System:
Character has HUD Displaying Health, Other Players Stats, Hunger and Thirst.
Containing shortcuts inventory

##### Control System:
Mouse and Keyboard



## Commands

##### Audio, Music, Sound FX:
* When the player is in fight mode, changing music into most intense one
* In the mist, some sound of monsters can be heard
* Character footstep are different according to the surface types


##### Help System:
None

## Artificial Intelligence:


##### Opponent and Enemy AI:
States:
* Roaming
* Chassing
* Combat
* Climbing

##### Non - CFC:
The player can kill other players

##### Support AI:
Ennemies AI are only alowed to move on Nav Mesh and navigation querry.

##### Target Hardware:
Wip

##### Development hardware and Software:
Windows and Unreal Engine

##### Network Requirements:
Wip



## Game Art:
Blender, Substance Painter/Designer, Photoshop
